#ifndef FONCTION_OPERATOR_IFSTREAM_H
#define FONCTION_OPERATOR_IFSTREAM_H

#include <string>
#include <stdexcept>
#include <ostream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>
#include <regex>
#include <type_traits>
#include <cctype>
#include <utility>
#include <typeinfo>

#include "../traits.hpp"

namespace libRabbit {


	// c'est template mais ça depend du fiat qu'on ai un "push back" 
	template<class T , class U = void >
	struct read_imp
	{
		static void read(std::istream& in, T& t)
		{
			std::string ligne;
			if ( in  && std::getline( in, ligne ) ) // ce test échoue si le fichier n'est pas ouvert 
			{
				typename T::value_type temp;
                std::insert_iterator<T> insert_ite (t, t.end());
				std::stringstream ssline(ligne);
                while ( ssline >> temp)
				{
                    insert_ite = temp;
				}
						
			} 
		}
	};

	template<class T>
	struct read_imp <T , 
						std::enable_if_t< is_container<typename T::value_type>::value && !is_string<typename T::value_type>::value>
					>
	{
		static void read(std::istream& in, T& t)
		{
			std::string ligne;
			if (in)
			{
                std::insert_iterator<T> insert_ite (t, t.end());
				while (  std::getline( in, ligne ) ) 
				{
					typename T::value_type temp;
					std::stringstream ssline(ligne);
                    ssline >> temp;
                    insert_ite = temp;
				} 
			}
		}
	};
    
	template<
			class T,
			class =  std::enable_if_t< is_container<T>::value >,
			class =  std::enable_if_t< !is_string<T>::value >
	>
	inline std::istream & operator>> (std::istream& in, T& container)
	{

		read_imp<T>::read(in,container);
		return in;
	}

}
#endif
