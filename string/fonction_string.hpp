#ifndef FONCTION_STRING_H
#define FONCTION_STRING_H

#include <string>
#include <stdexcept>
#include <ostream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>
#include <regex>
#include <type_traits>
#include <cctype>
#include <utility>

#include "../traits.hpp"

namespace libRabbit {

	// la version ou  la limite est un string;
	// l'entre est prise en copie, on peut l'utiliser directement
	// la limite n'est pas modifiée.
	inline std::vector<std::string> decoupe (std::string entre,const std::string& limite)
	{
		std::vector<std::string> retour;

		size_t pos = entre.find(limite);
		while (pos !=std::string::npos )
		{
			std::string temps =  entre.substr(0,pos);
			retour.push_back(temps);
			entre = entre.substr(pos+limite.size());
			pos = entre.find(limite);
		}
		retour.push_back(entre);
		return retour;
	}

	// decoupe avec "je-suis-ton-pere" en " je suis ton pere"
	inline std::vector<std::string>  decoupe (const std::string& chaine, const char* separateur)
	{
		return decoupe(chaine,std::string(separateur));
	}

	inline std::string to_upper (const std::string& chaine)
	{
		std::string retour;
		for (char c : chaine )
		{
			retour.push_back((char)toupper(c));
		}
		return retour;
	}

	inline std::string to_lower (const std::string& chaine)
	{
		std::string retour;
		for (char c : chaine )
		{
			retour.push_back((char)tolower(c));
		}
		return retour;
	}
}
#endif
