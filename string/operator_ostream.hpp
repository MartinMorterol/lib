#ifndef FONCTION_OPERATOR_OSTREAM_H
#define FONCTION_OPERATOR_OSTREAM_H

#include <string>
#include <stdexcept>
#include <ostream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>
#include <regex>
#include <type_traits>
#include <cctype>
#include <utility>

#include "../traits.hpp"

namespace libRabbit {
    
    template<class T,  class U = void >
	struct affiche_imp
	{
		static void  affiche(std::ostream& out, const T& t)
		{
			for (const auto& elem : t)
			{
				out << elem << " ";
			}
		}
	};

	template<class T>
			 
	struct affiche_imp <T , 
						std::enable_if_t< is_container<typename T::value_type>::value && !is_string<typename T::value_type>::value >
						
					>
	{
		static void  affiche(std::ostream& out, const T& t)
		{
			size_t i = 0;
			for (const auto& elem : t)
			{
				out <<"\t"<< elem ;
				if ( i != t.size()-1 ){
					out << "\n";
				}
				i++;
			}
		}
	};

	
	template<class T, class U>
	inline std::ostream& operator<< (std::ostream& out, const std::pair<T,U>& p)
	{
			out << "(" << p.first  << "," << p.second << ")" ;
			return out;
	}
	

	template<
			class T,
			class =  std::enable_if_t< is_container<T>::value >,
			class =  std::enable_if_t< !is_string<T>::value >
	>
	inline std::ostream & operator<< (std::ostream& out, const T& container)
	{
		affiche_imp<T>::affiche(out,container);
		return out;
	}



		


/*  Code faux mais interessant à garder
	* 
	//Base
	template<class T>
	void affiche(std::ostream& out, const T& t)
	{
		out << t << "! ";
	}
	
	
	// surchage : marche
	template<class T>
	void affiche(std::ostream& out, const std::vector<T>& t)
	{
		out <<"\t" << t << "!! \n";
	}

	// spécitialisation pour int (ok)
	template<>
	void affiche<int>(std::ostream& out, const int& t)
	{
		out << t << "! ";
	}

	// spé partielle: marche pas
	template<class T>
	void affiche<std::vector<T>>(std::ostream& out, const std::vector<T>& t)
	{
		out << t << "! ";
	}

	// Overload, pas une spécification 
	// pourquoi je m'en sort pas avec de l'a spécification
	/// => la spécialisation est partielle car on dit "vecteur<T>" est pas vecteur<int>
	template<class T>
	void affiche(std::ostream& out, const std::vector<T>& t)
	{
		out <<"\t" << t << "!! \n";
	}


*/
}
#endif
