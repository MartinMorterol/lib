#ifndef CONVERSION_HPP
#define CONVERSION_HPP

#include <sstream>
#include "traits.hpp"
#include "fonction_string.hpp"

namespace libRabbit {


	template <	class T	 >
	auto to_string(const T& t) -> std::enable_if_t<	std_to_stringable_t<T>::value ,
													std::string > 
	{
      return std::to_string(t);
    }
    
    template <	class T	 >
	std::enable_if_t<	! std_to_stringable_t<T>::value ,
						std::string
					> to_string(const T& t) {
       std::stringstream ss;
	   ss  <<  t;
	   return ss.str();
    }
	
    template <class T>
    T convert_to(const std::string& str) {
       std::stringstream ss(str);
       T temp;
       ss >> temp;
       return temp;
    }

    template <>
    double convert_to<double> (const std::string& str) {
        return std::atof(str.c_str());
    }

    template <>
    int convert_to<int> (const std::string& str) {
        return std::atoi(str.c_str());
    }

    template <>
    std::string convert_to<std::string> (const std::string& str) {
        return str;
    }
        
        

}


#endif