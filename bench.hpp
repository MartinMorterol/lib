#ifndef bench_HPP
#define bench_HPP

#include <iostream>
#include <chrono>

namespace libRabbit {
	
	template <int rep = 1 , class F, class... T>
	std::chrono::nanoseconds::rep exe_time (F&& fonction, T&&... param)
	{
		auto begin = std::chrono::high_resolution_clock::now();
		
		for (int i = 0 ; i < rep ; ++i)
		{
			fonction(std::forward<T>(param)...);
		}

		auto end = std::chrono::high_resolution_clock::now();
		return  std::chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count() ;
	}

	template <int rep = 1 ,class F, class... T>
	auto exe_time_on (F fonction, std::vector<T>... param)
	{
		auto for_all = [&]{
			auto borne = std::min( {param.size()...} );
			for (decltype(borne) i = 0 ; i < borne ; i ++ )
			{
				fonction(param[i]...);
			}
			
		};
		
		return exe_time<rep>(for_all);
	}
}

#endif
