#ifndef plot_HPP
#define plot_HPP

#include <iostream>
#include <vector>
#include <tuple>
#include <utility>
#include <fstream> 
#include <string>  

#include "convertion.hpp"
#include "bench.hpp"
#include "tuple.hpp"
#include "check_param.hpp"
#include <stdarg.h>


namespace libRabbit {


	
	template <class T = double>
	struct line {
		inline auto size() const { return values.size(); }
		std::string name;
		std::vector<T> values;
		line (std::string nom, std::initializer_list<T> val) : name (nom), values(val) {};
	};
	
	
	template <class T>
	std::ostream& operator << (std::ostream& out, const line<T>& e)
	{
		out << e.name << " " ;
		for ( decltype(e.size()) i = 0 ; i < e.size() ; i ++ )
		{
			out << e.values[i] ;
			if ( i != e.size() -1 )
			{
				out << " ";
			}
		}
		return out;
	}
	
	template<class Titres, class T = double>
	struct data_graphe 
	{
		std::vector<line<T>> lines;
		const Titres titres;
		
		data_graphe (const Titres& title) : titres(title) {}
		
		template <class ... Options>
		std::string generate_file (std::string nom_file, Options... opt )
		{
			std::ofstream file (nom_file);
			if (file)
			{
				file << *this;
			}
			std::string nom_plot = nom_file + "_gnu_plot";
			std::ofstream file_gnu (nom_plot);
			if (file_gnu)
			{
					std::string text = R"!(
						set terminal pdf truecolor
						set output ")!"+ nom_file +R"!(_graphe.pdf"
						set grid
						set style data histograms
						set style fill solid 1.00 border -1
						)!";
						
						auto concat = [&](auto option) {
							text += to_string(option) + "\n" ;
						};
						(void) concat; // mute the warning
						auto trick = std::initializer_list<int> {  (concat(opt),0)... };
						(void) trick;// mute the warning

						text += R"!(
						plot  )!"; 
					file_gnu  << text  <<  "\"" << nom_file << "\"" ;
					
					for (size_t i = 0 ; i < titres.size() ; ++i)
					{
							if ( i ==0 ) file_gnu << " using 2:xtic(1) title \"" << titres[0] << "\" ," ;
							else  file_gnu << "'' using "<< i+2 <<" title \"" << titres[i] << "\" ," ;
					}

			}
			
			return  "gnuplot "+nom_plot+" && xpdf "+nom_file +"_graphe.pdf ";
			
		}
	};
	
	template <size_t rep,class ...T> struct graphe {
		static_assert(false_<T...>::value,"Ne devrait pas être utilisé, use make_graphe() ");
	};
	

	template <size_t rep, class Titres, class ...Fn_>
	struct graphe<rep,Titres,std::tuple<Fn_...> >
	{
		std::tuple<Fn_...> fns;
		
		using exe_t = decltype(exe_time(0));
		using data_t = data_graphe<Titres,exe_t>; 
		data_t data;
		
		constexpr graphe (Titres nom, std::tuple<Fn_...> fn) : fns (fn), data(nom) {}
		
		template <class Nom, class... U >
		void run(Nom nom, U... u){
			return run_impl(nom, get_indexes(fns) ,u...);
		}
		
	private : 
		
		template <class Nom, size_t... I,class ... Args>
		void run_impl( Nom nom, std::index_sequence<I...>, Args... args) {
			data.lines.emplace_back( to_string(nom),
									 std::initializer_list<exe_t>(
										 { 	(
												check_param_if_possible(std::get<I>(fns),args...),
												(exe_t)(exe_time<rep>(std::get<I>(fns),args...)/(exe_t)rep)
											)... 
											 
										}) );
		}
		
	
	};
	
	template <size_t rep = 1, class ...T > 
	constexpr auto make_graphe (T... params) 
	{ 	
		static_assert( rep>0, "on doit faire au moins une répétition");
		return graphe<rep, decltype(to_array(make_tuple_even(params...))) , odd_tuple_t<T...> > (to_array(make_tuple_even(params...)), make_tuple_odd(params...)) ;
	}

	
	template<class tuple_string, class T = double>
	std::ostream& operator << (std::ostream& out, const data_graphe<tuple_string,T>& data)
	{
		for ( const auto& l : data.lines )
		{
			out << l << std::endl;
			
		}
		return out;
	}
	
	
	// Je suis pas sur que ça serve d'avoir un affichage a ce niveau la 
	template <size_t rep, class ... String_, class ...Fn_>
	std::ostream& operator << (std::ostream& out, const graphe<rep,std::tuple<String_...>,std::tuple<Fn_...> >& gr)
	{
		out << gr.data;
		return out;
	}
	


}

#endif
