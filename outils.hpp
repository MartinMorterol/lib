#ifndef OUITLS_HPP
#define OUITLS_HPP
#include <iostream>
#include <algorithm>
#include <vector>
#include <typeinfo>
#include <utility>
#include <fstream>

#include <random>



#include "fonction_string.hpp" 

namespace libRabbit {
    
    inline bool file_exists(std::string const & pathname)
    {
        std::ifstream f(pathname);
        return bool(f);
    }

    inline std::string get_name_file(const std::string chemin)
    {
        if ( chemin.find('/') != std::string::npos )
        {
            return decoupe(chemin,"/").back();
        }
        return chemin;
    }
    
    template <class T,class U>
    inline T random(const T& min, const U& max)
	{
		static std::random_device rd;     // only used once to initialise (seed) engine
		static std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
		
		std::uniform_int_distribution<T> uni(min,max); // guaranteed unbiased
		return uni(rng);
	}
	
	
	// pas aussi utile que je le souhaiterais :/
	template <bool, class T, size_t debut, size_t fin, size_t step  >
	struct for_impl;

	template <class T, size_t debut, size_t fin, size_t step  >
	struct for_impl <true,T,debut,fin,step>
	{
		constexpr static void do_ (T&& t)
		{
			t(std::integral_constant<size_t,debut>()); // je t'aime lénaic 
			for_impl<(debut+step<fin),T, debut+step, fin, step>::do_(std::forward<T>(t));
		}
	};

	template <class T, size_t debut, size_t fin, size_t step >
	struct for_impl <false,T,debut,fin,step>
	{
		constexpr static void do_ (T&&)
		{
			
		}
	};


	template <size_t debut, size_t fin, size_t step = 1,class T >
	constexpr void for_constexp (T&& fn)
	{
		for_impl<(debut<fin),T, debut, fin, step>::do_(std::forward<T>(fn));
	}
}

#endif

