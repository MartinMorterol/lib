#ifndef TUPLE_HPP
#define TUPLE_HPP

#include <utility>
#include <tuple>
#include <array>

#include "outils.hpp"
#include "traits.hpp"
namespace libRabbit 
{
	// from : http://stackoverflow.com/a/10766422 
	// implementation details, users never invoke these directly
	namespace detail
	{
		template <typename F, typename Tuple, bool Done, int Total, int... N>
		struct call_impl
		{
			static void call(F f, Tuple && t)
			{
				call_impl<F, Tuple, Total == 1 + sizeof...(N), Total, N..., sizeof...(N)>::call(f, std::forward<Tuple>(t));
			}
		};

		template <typename F, typename Tuple, int Total, int... N>
		struct call_impl<F, Tuple, true, Total, N...>
		{
			static void call(F f, Tuple && t)
			{
				f(std::get<N>(std::forward<Tuple>(t))...);
			}
		};
	}

	// user invokes this
	template <typename F, typename Tuple>
	void call(F f, Tuple && t)
	{
		typedef typename std::decay<Tuple>::type ttype;
		detail::call_impl<F, Tuple, 0 == std::tuple_size<ttype>::value, std::tuple_size<ttype>::value>::call(f, std::forward<Tuple>(t));
	}

	template<class T>
	constexpr std::make_index_sequence<
										std::tuple_size<T>::value
									>
	get_indexes( T const& ) { return {}; }
	
	
	
	// idée de : http://stackoverflow.com/a/6481167 (avec un bug :) )
	// push front for tuple
	template< class, class > struct push_front_tuple;

	template< class A, class... T > struct push_front_tuple< A, std::tuple< T... > > {
		using type = typename std::tuple< A, T... >;
	};
	
	

	// for even
	template< class... > struct even_tuple;

	template< > struct even_tuple<> {
		using type =  typename std::tuple< >;
	};
	
	template< class A > struct even_tuple< A > {
		using type =  typename std::tuple< A >;
	};
	template< class A, class B > struct even_tuple< A, B > {
		using type =  typename std::tuple< A >;
	};
	
	template< class A, class B, class... T > struct even_tuple< A, B, T... > {
		using type =  typename push_front_tuple< A, typename even_tuple< T... >::type >::type;
	};
	
	template< class... T>
	using even_tuple_t = typename even_tuple<T...>::type;
	
	// for odd
	template< class... > struct odd_tuple;


	template< > struct odd_tuple<> {
		using type =  typename std::tuple< >;
	};
	
	template< class A, class B > struct odd_tuple< A, B > {
		using type =  typename std::tuple< B>;
	};
	
	template< class A, class B, class C > struct odd_tuple< A, B, C > {
		using type =  typename std::tuple< B>;
	};

	template< class A, class B, class... T > struct odd_tuple< A, B, T... > {
		using type =  typename push_front_tuple< B, typename odd_tuple< T... >::type >::type;
	};
		
	template< class... T>
	using odd_tuple_t = typename odd_tuple<T...>::type;


	/// La version qui fait vraiment les tuples, et pas jsute le type, compile time :)
	
		
	template < size_t... I, class T, class Add >
	constexpr auto empile_elem_to_tuple_impl(std::index_sequence<I...>, T&& tuple, Add&& a) {
		return std::make_tuple(std::get<I>(tuple)...,std::forward<Add>(a));
	}

	template<class T, class Add>
	constexpr auto empile_elem_to_tuple (T&& tuple, Add&& a)
	{
		return empile_elem_to_tuple_impl(get_indexes(tuple), std::forward<T>(tuple) , std::forward<Add>(a) );
	}

	template <class Tuple, class ... T> struct make_tuple_even_impl{
		constexpr static auto add (Tuple&& tuple, T&&... t) {
						static_assert(false_<T...>::value,"ne doit pas etre utiliser");
				}	
	};

	template <class Tuple> 
	struct make_tuple_even_impl<Tuple> {
			constexpr static auto add (Tuple&& tuple) {
				return std::forward<Tuple>(tuple);
			}
	};

	template <class Tuple, class A> 
	struct make_tuple_even_impl<Tuple,A> {
			constexpr static auto add (Tuple&& tuple, A&& a) {
				return empile_elem_to_tuple(std::forward<Tuple>(tuple),std::forward<A>(a));
			}
	};


	template <class Tuple, class A, class B, class ... T> 
	struct make_tuple_even_impl<Tuple,A,B, T...> {
			constexpr static auto add (Tuple&& tuple, A&& a, B&&, T&&... t) {
				return make_tuple_even_impl< decltype (  empile_elem_to_tuple(std::forward<Tuple>(tuple),std::forward<A>(a)) ) ,
											T...>::add( empile_elem_to_tuple(std::forward<Tuple>(tuple),std::forward<A>(a)), 
														std::forward<T>(t)...);
			}
	};

	template <class ...T>
	constexpr auto make_tuple_even (T&&... args)
	{
		return make_tuple_even_impl<std::tuple<>,T...>::add(std::tuple<> (), std::forward<T>(args) ...);
	}


	template <class Tuple, class ... T> struct make_tuple_odd_impl{
		constexpr static auto add (Tuple&& tuple, T&&... t) {
					static_assert(false_<T...>::value,"ne doit pas etre utiliser");
				}	
	};

	template <class Tuple> 
	struct make_tuple_odd_impl<Tuple> {
			constexpr static auto add (Tuple&& tuple) {
				return std::forward<Tuple>(tuple);
			}
	};

	template <class Tuple, class A> 
	struct make_tuple_odd_impl<Tuple,A> {
			constexpr static auto add (Tuple&& tuple, A&&) {
				return std::forward<Tuple>(tuple);
			}
	};


	template <class Tuple, class A, class B, class ... T> 
	struct make_tuple_odd_impl<Tuple,A,B, T...> {
			constexpr static auto add (Tuple&& tuple, A&& , B&& b, T&&... t) {
				return make_tuple_odd_impl< decltype (  empile_elem_to_tuple(std::forward<Tuple>(tuple),std::forward<B>(b)) ) ,
											T...>::add( empile_elem_to_tuple(std::forward<Tuple>(tuple),std::forward<B>(b)), 
														std::forward<T>(t)...);
			}
	};


	template <class ...T>
	constexpr auto make_tuple_odd (T&&... args)
	{
		return make_tuple_odd_impl<std::tuple<>,T...>::add(std::tuple<> (), std::forward<T>(args) ...);
	}


	
	/// source A REGARDER http://stackoverflow.com/a/10615119
	
		template<int... Indices>
	struct indices {
		using next = indices<Indices..., sizeof...(Indices)>;
	};

	template<int Size>
	struct build_indices {
		using type = typename build_indices<Size - 1>::type::next;
	};

	template<>
	struct build_indices<0> {
		using type = indices<>;
	};

	template<typename T>
	using Bare = typename std::remove_cv<typename std::remove_reference<T>::type>::type;

	template<typename Tuple>
	constexpr
	typename build_indices<std::tuple_size<Bare<Tuple>>::value>::type
	make_indices()
	{ return {}; }

	template<typename Tuple, int... Indices>
	std::array<
	typename std::tuple_element<0, Bare<Tuple>>::type,
		std::tuple_size<Bare<Tuple>>::value
	>
	to_array(Tuple&& tuple, indices<Indices...>)
	{
		using std::get;
		return {{ get<Indices>(std::forward<Tuple>(tuple))... }};
	}

	template<typename Tuple>
	auto to_array(Tuple&& tuple)
	-> decltype( to_array(std::declval<Tuple>(), make_indices<Tuple>()) )
	{
		return to_array(std::forward<Tuple>(tuple), make_indices<Tuple>());
	}
	
	
	// affichage utile :)
	template<class T>
	void display_tuple (T tt)
	{
		for_constexp<0, std::tuple_size< T>::value>([&](auto const i){cout << std::get<i>(tt) << endl;}); 
	}
}
#endif