#ifndef Poly_hpp
#define Poly_hpp

#include <utility>
#include <type_traits>
#include <cmath>
#include <iostream>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/linestring.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/geometries.hpp>
	
namespace libRabbit {

	template <class T>
	using point_t = boost::geometry::model::d2::point_xy<T>;
	
	template <class T>
	using polygone_t = boost::geometry::model::polygon<point_t<T>>;
	
    using point = boost::geometry::model::d2::point_xy<float>;
    using polygone = boost::geometry::model::polygon<point>;
	
	template <class T>
	inline T getX(point_t<T> t)  { return t.x();}

	template <class T>
	inline T getY(point_t<T> t)  { return t.y();}
	
	template <class T>
	point_t<T> operator* (const point_t<T>& p1 ,const T& scalaire )  { return {p1.x()*scalaire,p1.y()*scalaire}; }
	template <class T>
	point_t<T> operator+ (const point_t<T>& p1 ,const T& scalaire )  { return {p1.x()+scalaire,p1.y()+scalaire}; }
	
	template <class T>
	point_t<T> operator- (const point_t<T>& p1 ,const point_t<T>& p2)  { return {p1.x()-p2.x(),p1.y()-p2.y()}; }
	template <class T>
	point_t<T> operator+(const point_t<T>& p1 ,const point_t<T>& p2)  { return {p1.x()+p2.x(),p1.y()+p2.y()}; }
	
	
        template<class T>
	auto make_poly (const T& list_point)
	{
		polygone poly;
		for (auto& pt : list_point)
		{
			boost::geometry::append(poly.outer(), pt);
		}
		return poly;
	}
	
	
	template<class Point, class Poly = polygone>
	Poly translate_ (const Point& to,const Poly& poly )
	{
		    namespace trans = boost::geometry::strategy::transform;
			trans::translate_transformer<double, 2, 2> translate(getX(to), getY(to) );
			Poly result;
			boost::geometry::transform(poly, result, translate);
			return result;
	}
	
	polygone translate (const point& to,const polygone& poly )
	{
		return translate_(to,poly);

	}
	point translate (const point& to,const point& poly )
	{
		return translate_(to,poly);

	}
	
	template<class Point, class T, class Poly = polygone>
	Poly rotate_ (const Point& centre, T angle_deg, Poly poly )
	{
		    namespace trans = boost::geometry::strategy::transform;
			
			trans::translate_transformer<double, 2, 2> translate_to_origine(-getX(centre), -getY(centre) );
			trans::translate_transformer<double, 2, 2> translate_to_centre ( getX(centre),  getY(centre) );
			trans::rotate_transformer<boost::geometry::degree, double, 2, 2> rotate(angle_deg);
			Poly result;
			boost::geometry::transform(poly, result, translate_to_origine);
			boost::geometry::transform(result, poly, rotate);
			boost::geometry::transform(poly, result, translate_to_centre);
			
			return result;
	}
	polygone rotate (const point& centre, float angle_deg, polygone poly )
	{
		return rotate_(centre,angle_deg,poly);
	}
	
	point rotate (const point& centre, float angle_deg, point poly )
	{
		return rotate_(centre,angle_deg,poly);
	}
	
	// je pense que ça sert a rien :)
	template <size_t cote,class Point>//, class T = float>
	auto make_polygone_regulier (Point centre, float rayon) 
	{
			polygone sommets;
			double angle = 0.;
			double alpha = convertion_deg_rad(360. / (double) cote);
			
			// on commence avec l'angle de 0
			boost::geometry::append(sommets.outer(), point(rayon + getX(centre) , getY(centre) ) );
			// on ajoute les angles par pas de alpha
			for ( size_t i = 1 ; i < cote ; i ++ )
			{
				angle += alpha;
				float x = (float)std::cos(angle) * rayon + getX(centre);
				float y = (float)std::sin(angle) * rayon + getY(centre);
				boost::geometry::append(sommets.outer() , point(x,y) );
				
			}
			// et on fini la ou on commence
			boost::geometry::append(sommets.outer(), point(rayon + getX(centre) , getY(centre) ) );
			
			// TODO pourquoi j'ai besoin de ça
			boost::geometry::correct(sommets);
			return sommets;
	}
			
	auto make_triangle (point centre, float rayon)
	{
		return make_polygone_regulier<3>(centre,rayon);
	}

	auto make_carre (point centre, float rayon)
	{
		return make_polygone_regulier<4>(centre,rayon);
	}
	
	auto make_hexagone (point centre, float rayon)
	{
		return make_polygone_regulier<6>(centre,rayon);
	}
	
	auto make_octogone (point centre, float rayon)
	{
		return make_polygone_regulier<8>(centre,rayon);
	}
	
	


}
#endif

