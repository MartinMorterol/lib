#ifndef POINT_HPP
#define POINT_HPP

#include <utility>
#include <type_traits>

#include "../traits.hpp"

namespace libRabbit {
	template <class T, class U = void, class V = void, class C = void, class X = void >
	struct is_point : std::false_type {};
	

	// On fournis une version de getX et getY pour toutes classe ayant un x et un y public
	template <class T>
	inline decltype(std::declval<T>().x) getX(T&& t)  { return t.x ;}

	template < class T>
	inline decltype(std::declval<T>().y) getY(T&& t)  { return t.y ;}

	template <class T>
	inline void setX(T& t, decltype(std::declval<T>().x) value )  { t.x = value ;}

	template < class T>
	inline void setY(T& t, decltype(std::declval<T>().y) value )  { t.y = value ;}

	template <class T>
	struct is_point<   T,
                        typename void_if_valide<decltype(getX(std::declval<T>()))>::type,
                        typename void_if_valide<decltype(getY(std::declval<T>()))>::type,
                        typename void_if_valide<decltype(setX(std::declval<T>(),std::declval<decltype(getX(std::declval<T>()))>()))>::type,
                        typename void_if_valide<decltype(setY(std::declval<T>(),std::declval<decltype(getY(std::declval<T>()))>()))>::type
				>  : std::true_type {};
				
				
	/***
	 *
	 *
	 * Fait la conversion d'un point à un autre. globalement ça sert a dire au compilateur qu'on sais ce qu'on fait
	 * Et qu'on veux pas de warnings
	 *
	 *  
	 ***/
	template <  	class PointCible,
                    class PointFrom,
                    class X = typename std::enable_if< is_point<PointCible>::value >::type,
                    class W = typename std::enable_if< is_point<PointFrom>::value >::type>
	PointCible convert_to (const PointFrom & from)
	{
			using type_cible_x = decltype ( getX ( std::declval<PointCible>() ));
			using type_cible_y = decltype ( getY ( std::declval<PointCible>() ));
			return { type_cible_x(getX(from)) , type_cible_y(getY(from)) };
	}
	
	
	// Sans doute a deplacer dans un fichier point
	template<   class Point,
				class = std::enable_if_t< is_point<Point>::value >
					>
	inline std::ostream& operator<< (std::ostream& out, const Point& p)
	{

			out << "(" << getX(p)  << "," << getY(p) << ")" ;

			return out;
	}

}





#endif

