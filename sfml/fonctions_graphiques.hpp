#ifndef FONCTIONS_GRAPHIQUES
#define FONCTIONS_GRAPHIQUES


#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>
#include <typeinfo>

#include "../traits.hpp"
#include "../math/point.hpp"


namespace libRabbit {

	/// Me faire une fiche pour ce genre de cas la, j'ai toujours du mal a connaitre la "méthode propre"
	template <class Point,
			  class U = std::enable_if_t < 
											is_point<Point>::value &&
											!is_container<Point>::value
										 >
			 >
	void draw (sf::RenderWindow& window, sf::Shape& shape, const Point& point)
	{
		shape.setPosition((float)getX(point),(float)getY(point));
		window.draw(shape);
	}
	
	
	template <class ContPoint,
			  class U = std::enable_if_t <
											is_container<ContPoint>::value
										 >,
			  class Z = U
			 >
	void draw (sf::RenderWindow& window, sf::Shape& shape, const ContPoint& points)
	{
		for (const auto& point : points )
		{
			draw(window,shape,point);
		}
	}
	/// fin à refactor
	
	template <class Conteneur = std::vector<sf::Vector2f> >
	sf::VertexArray create_lines (Conteneur && points, const sf::Color couleur =  sf::Color::White)
	{
		sf::VertexArray lines (sf::LinesStrip, points.size() );
		
		// définit les points
		size_t cpt = 0 ;
		for (auto && point : points )
		{
			lines[cpt].position = sf::Vector2f(getX(point), getY(point));
			
			// la couleur 
			lines[cpt].color = couleur;
			
			cpt++;
		}
		
	
		return lines;
	}
	

	// sempai, static assert moi !!
	template<class Conteneur = std::vector<sf::Vector2f> >
	sf::ConvexShape create_polygone (Conteneur && points)
	{
		static_assert(is_container<Conteneur>::value, "Le parametre n'est pas un conteneur");
		static_assert(is_point<decltype (points[0])>::value, "Le conteneur ne contient pas de point");
		
		sf::ConvexShape convex;
		convex.setPointCount(points.size());

		// définit les points
		size_t cpt = 0 ;
		for (auto && point : points )
		{
			convex.setPoint(cpt, sf::Vector2f(getX(point), getY(point)));
			cpt++;
		}

		return convex;
	}
	
	
}
#endif
 
