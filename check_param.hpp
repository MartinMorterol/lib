#pragma once
#ifndef check_param_hpp
#define check_param_hpp



#include <algorithm>
#include <functional>
#include <type_traits>
#include <utility>

#include "traits.hpp"
#include "tuple.hpp"
#include "paramInfo.hpp"


namespace libRabbit {
	
	

	template <bool b,class T, class ... Params>
	struct check_param_if_possible_impl 
	{
		static void check()
		{
			// vide, on peut pas verifier dans ce cas !
		}

	};



	template <class T, class U, class ... P>
	struct check_param{
		static_assert(false_<T>::value,"class de base uniquement pour faire une spéssification, ne devrait jamais etre créer");
	};

	template < class T , class U>
	void check_param_impl ()
	{
		static_assert(std::is_same<std::decay_t<T>,std::decay_t<U>>::value,"type params incorecte");
	}


	template <size_t ... I , class T, class ... P>
	struct check_param<T,std::index_sequence<I...>,P...>{
		static void check()
		{
			std::initializer_list<int>({ (check_param_impl < type_param<I,T>,P	>(),0)... });
		}
	};

	template <class T, class ... Params>
	struct check_param_if_possible_impl <true,T,Params...>
	{
		static void check()
		{
			static_assert(nbParam<T>()==sizeof...(Params),"Nombres de params incorecte");
			check_param<	 T,
						std::make_index_sequence<sizeof...(Params)>,
						Params...
					>::check();
		}
	};


	template <class T, class ... Params>
	auto check_param_if_possible (T , Params ...)
	{
		return check_param_if_possible_impl<
												can_be_checked<std::remove_pointer_t<T>>::value,
												T,
												Params...
											>::check();
	}

	
	
	
}
#endif 
